import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { Project } from '../projects/project.entity';
import { Task } from '../tasks/task.entity';

@ObjectType()
@Entity()
export class User {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ nullable: false })
  password: string;

  @Field()
  @Column({ nullable: false })
  username: string;

  @Field()
  @Column({ nullable: false })
  name: string;

  @Field((_type) => [Project], { nullable: 'items' })
  @OneToMany((_type) => Project, (project) => project.user)
  projects?: Project[];

  @Field((_type) => [Task], { nullable: 'items' })
  @OneToMany((_type) => Task, (task) => task.user)
  tasks?: Task[];

  @Field()
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Field()
  @Column()
  @UpdateDateColumn()
  updated_at: Date;
}
