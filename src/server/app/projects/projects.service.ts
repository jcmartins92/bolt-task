import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateProjectDto, UpdateProjectDto } from './dto/create-project.dto';
import { Project } from './project.entity';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private projectRepository: Repository<Project>,
  ) {}

  create(project: CreateProjectDto) {
    return this.projectRepository.save(project);
  }

  async update(id, data: UpdateProjectDto) {
    const project = await this.findOne({ where: { id } });
    if (project) {
      project.alias = data.alias;
      return this.projectRepository.save(project);
    }
  }

  async delete(id) {
    const project = await this.findOne({ where: { id } });
    if (project) {
      const deleteResult = await this.projectRepository.remove(project);
      return deleteResult;
    }
  }

  findOne(params: FindOneOptions<Project> = {}) {
    return this.projectRepository.findOne(
      Object.assign({ relations: ['user', 'tasks'] }, params),
    );
  }

  findAll(params: FindManyOptions<Project> = {}) {
    return this.projectRepository.find(
      Object.assign({ relations: ['user', 'tasks'] }, params),
    );
  }

  async findOrCreateOne(params: FindOneOptions<Project> = {}) {
    let project: Project;

    project = await this.findOne(params);
    if (!project) {
      const conditions = params.where as CreateProjectDto;
      project = await this.create({
        alias: conditions.alias,
        user: conditions.user,
      });
    }
    return project;
  }

  async createProject(params: CreateProjectDto) {
    return this.findOrCreateOne({
      where: { user: params.user, alias: params.alias },
    });
  }
}
