import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from '../users/user.entity';
import { Task } from '../tasks/task.entity';

@ObjectType()
@Entity()
export class Project {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ nullable: false })
  alias: string;

  @Field((_type) => User)
  @ManyToOne((_type) => User, (user) => user.projects, { nullable: false })
  user: User;

  @Field((_type) => [Task], { nullable: 'itemsAndList' })
  @OneToMany((_type) => Task, (task) => task.project, { onDelete: 'CASCADE' })
  tasks?: Task[];

  @Field()
  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Field()
  @Column()
  @UpdateDateColumn()
  updated_at: Date;
}
