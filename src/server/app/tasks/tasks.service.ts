import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { CreateTaskDto, CreateTaskFromProjectDetailsDto } from './dto/create-task.dto';
import { Task } from './task.entity';
import { ProjectsService } from '../projects/projects.service';
import { TaskCompleteException } from './exeception/TaskCompleteException';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private tasksRepository: Repository<Task>,
    @Inject(ProjectsService) private projectsService: ProjectsService,
  ) {
  }

  create(task: CreateTaskDto) {
    return this.tasksRepository.save(task);
  }

  findOne(params: FindOneOptions<Task> = {}) {
    return this.tasksRepository.findOne(
      Object.assign({ relations: ['user', 'project'] }, params),
    );
  }

  findAll(params: FindManyOptions<Task> = {}) {
    return this.tasksRepository.find(
      Object.assign(
        {
          relations: ['user', 'project'],
          order: {
            complete: 'ASC',
            complete_date: 'DESC',
            created_at: 'ASC',
          },
        },
        params,
      ),
    );
  }

  async findOrCreateOne(params: FindOneOptions<Task> = {}) {
    let task: Task;
    task = await this.findOne(params);
    if (!task) {
      const conditions = params.where as CreateTaskDto;
      task = await this.create({
        subject: conditions.subject,
        user: conditions.user,
        complete: conditions.complete,
        complete_date: conditions.complete ? new Date() : null,
        project: conditions.project,
      });
    }
    return task;
  }

  async createFromProjectDetails(params: CreateTaskFromProjectDetailsDto) {
    const project = await this.projectsService.findOne({
      where: { id: params.projectId },
    });
    return this.findOrCreateOne({
      where: {
        user: params.user,
        subject: params.subject,
        complete: params.complete,
        project: project,
      },
    });
  }

  async remove(id) {
    const project = await this.findOne({ where: { id } });
    if (project) {
      return await this.tasksRepository.remove(project);
    }
  }

  async toggleTask(id) {
    const task = await this.findOne({ where: { id } });
    if (task.complete) {
      throw new TaskCompleteException();
    }
    if (task) {
      task.complete = !task.complete;
      task.complete_date = task.complete ? new Date() : null;
      return await this.tasksRepository.save(task);
    }
  }

  async updateSubject(id, subject) {
    const task = await this.findOne({ where: { id } });
    if (task) {
      task.subject = subject;
      return await this.tasksRepository.save(task);
    }
  }
}
