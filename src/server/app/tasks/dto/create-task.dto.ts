import { User } from '../../users/user.entity';
import { Project } from '../../projects/project.entity';

export class CreateTaskDto {
  subject: string;
  complete: boolean;
  complete_date?: Date;
  user: User;
  project: Project;
}

export class CreateTaskFromProjectDetailsDto {
  subject: string;
  complete: boolean;
  user: User;
  projectId: number;
}
