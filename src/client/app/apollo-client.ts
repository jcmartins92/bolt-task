import {
  ApolloClient,
  gql,
  InMemoryCache,
  MutationHookOptions,
  QueryHookOptions,
  useMutation,
  useQuery,
} from '@apollo/client';
import { Request } from 'express';
import { MapType, Query, ValueTypes, Zeus } from './types/graphql-zeus';
const client = new ApolloClient({
  uri: 'http://localhost:3333/graphql',
  cache: new InMemoryCache(),
});

export const context = (req: Request) => {
  return {
    headers: {
      authorization: `Bearer ${req.cookies['jwt']}`,
    },
  };
};

export const typedQuery = async <Q extends ValueTypes['Query']>(
  query: Q,
  req: Request,
) => {
  const { data } = await client.query({
    query: gql(Zeus.query(query)),
    context: context(req),
  });
  const typedData = data as MapType<Query, Q>;
  return typedData;
};

export const useTypedQuery = <Q extends ValueTypes['Query']>(
  query: Q,
  options?: QueryHookOptions<MapType<Query, Q>, Record<string, any>>,
) => {
  return useQuery<MapType<Query, Q>>(gql(Zeus.query(query)), options);
};

export function useTypedMutation<Q extends ValueTypes['Mutation']>(
  mutation: Q,
  options?: MutationHookOptions<MapType<Query, Q>, Record<string, any>>,
) {
  return useMutation<MapType<Query, Q>>(gql(Zeus.mutation(mutation)), options);
}
export default client;
