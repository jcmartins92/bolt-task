import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import TaskActionContext from '../../context/TaskActionContext';
import ProjectContext from '../../context/ProjectContext';
import TaskForm from './TaskForm';
import { useTaskCreate } from './form';

export default function TaskCreate({}) {
  const { id } = useContext(ProjectContext);
  const { onCreate } = useContext(TaskActionContext);
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
    setError,
  } = useForm({ defaultValues: { subject: '' } });
  const createTask = useTaskCreate({
    id,
    onChange: () => {
      onCreate();
      reset();
    },
    setError,
  });
  return (
    <TaskForm
      errors={errors}
      register={register}
      onSubmit={handleSubmit((data) => {
        createTask(data.subject);
      })}
    />
  );
}
