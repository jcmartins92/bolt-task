import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import TaskContext from '../../context/TaskContext';
import TaskActionContext from '../../context/TaskActionContext';
import { useTypedMutation } from '../../../app/apollo-client';
import { Delete } from '@material-ui/icons';
import { toast } from 'react-toastify';

export default function TaskDelete() {
  const { id } = useContext(TaskContext);
  const { onDelete } = useContext(TaskActionContext);
  const [open, setOpen] = useState(false);
  const [removeTask] = useTypedMutation(
    {
      removeTask: [
        {
          id: id,
        },
        { complete: true },
      ],
    },
    {
      onCompleted: () => {
        toast(`Removed`);
        onDelete();
      },
      onError: (error) => {
        toast.error(`something wrong ${error.message}`);
      },
    },
  );
  const onDeleteConfirm = () => {
    removeTask({});
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <IconButton color={'secondary'} onClick={handleClickOpen}>
        <Delete />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby={'project-remove'}
        aria-describedby={'project-remove-description'}
      >
        <DialogTitle id="project-remove">Remove task</DialogTitle>
        <DialogContent>
          <DialogContentText id="project-remove-description">
            This action is irreversible
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onDeleteConfirm} color="secondary">
            Remove
          </Button>
          <Button autoFocus={true} onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
