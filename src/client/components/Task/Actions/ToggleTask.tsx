import React, { useContext } from 'react';
import TaskContext from '../../context/TaskContext';
import TaskActionContext from '../../context/TaskActionContext';
import Switch from '@material-ui/core/Switch';
import { useLovelySwitchStyles } from '@mui-treasury/styles/switch/lovely';
import { useToggleTask } from './form';
import { toast } from 'react-toastify';

export default function ToggleTask({}) {
  const lovelyStyles = useLovelySwitchStyles();
  const { id, complete } = useContext(TaskContext);
  const { onUpdate } = useContext(TaskActionContext);
  const toggleTask = useToggleTask({ id, onChange: onUpdate });
  const onToggle = () => {
    if (!complete) {
      toggleTask();
    } else {
      toast(`You cannot reopen a task`);
    }
  };
  return (
    <Switch
      classes={lovelyStyles}
      checked={complete}
      onChange={() => onToggle()}
    />
  );
}
