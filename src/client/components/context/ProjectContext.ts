import { createContext } from 'react';
import { DateTime, User } from '../../app/types/graphql-zeus';

type ProjectTask = { complete: boolean };
type ProjectContext = {
  id?: number;
  alias?: string;
  user?: User;
  tasks?: null | ProjectTask[];
  created_at?: DateTime;
  updated_at?: DateTime;
};
export default createContext<ProjectContext>({});
