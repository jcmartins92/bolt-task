import { createContext } from 'react';

export interface TaskAction {
  onUpdate?: () => void;
  onCreate?: () => void;
  onDelete?: () => void;
}

export default createContext<TaskAction>({});
