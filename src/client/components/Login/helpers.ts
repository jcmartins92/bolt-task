import { toast } from 'react-toastify';
import get from 'lodash/get';

export function responseToastError(error) {
  if (get(error, 'response.data.message')) {
    toast.error(get(error, 'response.data.message'));
  } else if (get(error, 'message')) {
    toast.error(`Is not supposed, graphql.. ${get(error, 'message')}`);
  } else {
    toast.error(`Is not supposed, maybe later will work.. ${error}`);
  }
}
